import java.util.Scanner;

public class SurgeryMenu {
	//creates an insatnce of serialisation
	Serialization serializer = new Serialization();
	//creates a new scanner
	 private static final Scanner S = new Scanner(System.in);
	 //declaring variables
	private static Surgery surgery;
	static Surgery currentSurgery;

		    
		//this method gets the current surgery and returns it
	    public static Surgery getCurrentSurgery() {
	        return surgery= currentSurgery;
	    }
	    
	    //method to accept a user's choice
	    public void choice() {
		String choice = "";  {
			//menu display
			System.out.println("-- " + surgery.toString() + "'s SURGERY MENU --");
			System.out.println("1 : makePet");
			System.out.println("2 : getPetByName");
			System.out.println("3 : makeBooking");
			System.out.println("4 : getPetsNextBooking");
			System.out.println("5 : ToDo");
			System.out.println("Q : Quit");
			System.out.print("Pick : ");
	
			//Accepts the user's input and converts it to upper case and saves it to choice
			choice = S.nextLine().toUpperCase();
	
			//what happens based on the user's choice
			switch (choice) {
				case "1" : {
					PetManager.makePet();
					break;
				}
				case "2" : {
					PetManager.getPetByName();
					break;
				}
				case "3" : {
					Surgery.makeBooking();
					break;
				}
				case "4" : {
					Surgery.getPetsNextBooking();
					break;
				}
				case "5" : {
					// ToDo : ?
					break;
				}
			}
	
		}{ while (!choice.equals("Q"));
	
		// NOTE : Logging Out !
		surgery = null;
	}
	
	}
}
	

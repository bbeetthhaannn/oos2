import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import ljmu.vets.Booking;
import ljmu.vets.Pet;
import java.util.ArrayList;
import java.util.List;

public class Surgery implements PetRepository {
	//Declares a new scanner
	 private static final Scanner S = new Scanner(System.in);
	 //creates a pet array
	 private List<Pet> pets = new ArrayList<Pet>();
	 
	public Surgery(String string) {
	}
	//adds a pet
	public void addPet(Pet pet) {
        pets.add(pet);
    }
	//returns pets to an array
	public Pet[] getPets() {
        return pets.toArray(new Pet[pets.size()]);
    }
	//makes a booking
	static void makeBooking() {
		System.out.print("Booking's Ref : ");
		String ref = S.nextLine();

		System.out.print("Pet's Name : ");
		String name = S.nextLine();

		Pet pet = PetManager.getPetByName();
		if (pet == null) {
			System.out.println("Pet not found.");
			return;
		}
		//outputs the booking information
		System.out.print("Booking's DateTime [i.e. 03 Oct 23 09:30] : ");
		LocalDateTime when = LocalDateTime.parse(S.nextLine(), DateTimeFormatter.ofPattern("dd MMM yy HH:mm"));

		System.out.print("Booking's Duration [i.e. MINS] : ");
		Integer duration = Integer.parseInt(S.nextLine());

		Booking booking = new Booking(ref, pet, when, duration);
	    pet.makeBooking(booking);
	}
	//gets a pets next booking
	static void getPetsNextBooking() {
		System.out.print("Pet's Name : ");
		String name = S.nextLine();

		// ToDo : Validate ?
		Pet pet = PetManager.getPetByName();

		try {
			if (Pet.getNextBooking() != null) {
			    System.out.println(pet.getNextBooking().getWhen().format(DateTimeFormatter.ofPattern("dd MMM yy HH:mm")));
			} else {
			    System.out.println("Pet has no bookings.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

interface PetRepository {
    void addPet(Pet pet);
    Pet[] getPets();
}




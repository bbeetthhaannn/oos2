//importing packages needed
import java.util.ArrayList;
import java.util.Scanner;

public class EntryMenu {
	//creates an instance of serialisation
	Serialization serializer = new Serialization();
	//creates a new scanner
	 private static final Scanner S = new Scanner(System.in);
	private static Surgery currentSurgery;

		    
		//gets the current surgery and returns it
	    public static Surgery getCurrentSurgery() {
	        return currentSurgery;
	    }
	String choice = "";
	
	//creates a new array for surgeries to be stored in
	ArrayList<Object> surgeries = new ArrayList<Object>(); {
		//checksif surgeries is empty or not
	if (surgeries.isEmpty()) {
        System.out.println("No surgeries found. Please add a surgery.");

    }

	do {
		//if its not empty it displays an option menu
		System.out.println("-- ENTRY MENU --");
		System.out.println("1 : [L]ogOn");
		System.out.println("Q : Quit");
		System.out.print("Pick : ");
		//takes the user's input
		choice = S.nextLine().toUpperCase();
		//goes to log on if user chooses to
		switch (choice) {
			case "1" :
			case "L" : {
				LogOnService logOnService = new LogOnService();
                logOnService.logOn();
                currentSurgery = SurgeryMenu.getCurrentSurgery();
                break;
			}
		}
		//quits if user chooses to
	} while (!choice.equals("Q"));

	serializer.serialize();

	System.out.println("Bye Bye !");

	System.exit(0);

}
}

import java.awt.Menu;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import ljmu.vets.Pet;

public class PetManager {
	//creates a new scanner
	 private static final Scanner S = new Scanner(System.in);
	static void makePet() {
		//accepts the user's input of the pets name
		System.out.print("Pet's Name : ");
		String name = S.nextLine();

		//accepts the input of the pets reg date
		System.out.print("Pet's RegDate [i.e. 03 Oct 23] : ");
		LocalDate regDate = LocalDate.parse(S.nextLine(), DateTimeFormatter.ofPattern("dd MMM yy"));
		 Pet pet = new Pet(name, regDate);
		 //adds the pet
		 if (SurgeryMenu.getCurrentSurgery() != null) {
				SurgeryMenu.getCurrentSurgery().addPet(pet);
			} else {
				System.out.println("No surgery is currently selected.");
			}
		}
	//accepts user input of the pets name
	static Pet getPetByName() {
		System.out.print("Pet's Name : ");
		String name = S.nextLine();

		 // search for the pet by name in the list of pets
	    for (Pet pet : SurgeryMenu.getCurrentSurgery().getPets()) {
	        if (pet.getName().equals(name)) {
	            System.out.println(pet.toString());
	            return pet;
	        }
	    }

	    // pet not found
	    System.out.println("Pet not found.");
	    return null;
	}

}

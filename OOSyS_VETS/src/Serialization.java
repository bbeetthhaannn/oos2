import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import ljmu.vets.Booking;
import ljmu.vets.Cat;
import ljmu.vets.Pet;
import ljmu.vets.Surgery;

public class Serialization {
	//specifies a file pth
	private final static String PATH = "M:\\data\\OOSyS\\";
	//creates an array called surgeries
	private ArrayList<Surgery> surgeries;
	//adding example data
	public void examples() {
	    this.surgeries = new ArrayList<Surgery>();
	    
	    surgeries.add(new Surgery("SurgeryA"));
		surgeries.add(new Surgery("SurgeryB"));

		Pet pet1 = new Cat("GlynH", LocalDate.of(2020, 11, 11));
		surgeries.get(0).makePet(pet1);

		Booking booking1 = new Booking("SurgeryA-REF1", pet1, LocalDateTime.of(2022, 9, 9, 9, 33), 30);
		surgeries.get(0).makeBooking(booking1);

		Booking booking2 = new Booking("SurgeryA-REF2", pet1, LocalDateTime.of(2023, 4, 4, 9, 22), 60);
		surgeries.get(0).makeBooking(booking2);

		Booking booking3 = new Booking("SurgeryA-REF3", pet1, LocalDateTime.of(2025, 11, 11, 11, 00), 90);
		surgeries.get(0).makeBooking(booking3);
	}

	@SuppressWarnings("unchecked")
	public void deSerialize() {
		ObjectInputStream ois;

		try {
			ois = new ObjectInputStream(new FileInputStream(PATH + "surgeries.ser"));

			// NOTE : Erasure Warning !
			this.surgeries = (ArrayList<Surgery>)ois.readObject();

			// ToDo : Finally ?
			ois.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void serialize() {
		ObjectOutputStream oos;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(PATH + "surgeries.ser"));
			oos.writeObject(this.surgeries);

			// ToDo : Finally ?
			oos.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}


}

package ljmu.vets;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Booking implements BookingInterface, Serializable {
	//declaring variables
	private String ref;
	private PetInterface pet;
	private LocalDateTime when;
	private Integer duration;

	public Booking(String ref, Pet pet, LocalDateTime when, Integer duration) {
		//getting the data needed
		this.ref = ref;
		this.pet = pet;
		this.when = when;
		this.duration = duration;
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + " >> " +
				this.ref + " " +
				this.pet.toString() + " " +
				this.when.format(DateTimeFormatter.ofPattern("dd MMM yy HH:mm"));
	}

	public String getRef() {
		return this.ref;
	}

	public PetInterface getPet() {
		return this.pet;
	}

	public LocalDateTime getWhen() {
		return this.when;
	}

	public int getDuration() {
		return this.duration;
	}

	public String getRefCode() {
		return null;
	}

	// ToDo : get / set Methods ?
}

interface PetInterface {
    public String getName();
    public LocalDate getRegDate();
    public void makeBooking(BookingInterface when);
    public BookingInterface getNextBooking();
    public String toString();
}





package ljmu.vets;

import java.time.LocalDateTime;

public interface BookingInterface {
	    
	    // Returns the reference code for the booking
	    String getRefCode();
	    
	    // Returns the pet associated with the booking
	    PetInterface getPet();
	    
	    // Returns the date and time of the booking
	    LocalDateTime getWhen();
	    
	    // Returns the duration of the booking in minutes
	    int getDuration();
	    
	    // Returns a string representation of the booking
	    String toString();
	}



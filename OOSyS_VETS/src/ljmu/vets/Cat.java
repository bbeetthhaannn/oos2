package ljmu.vets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Cat extends Pet {
	//sets cat variables
	public Cat(String name, LocalDate regDate) {
		super(name, regDate);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " >> " +
				this.name + " " +
				this.regDate.format(DateTimeFormatter.ofPattern("dd MMM yy"));
	}

	// ToDo : get / set Methods ?
}

package ljmu.vets;

public class PublicMedicine extends SurgeryMedicine {
	//declares a variable
	private Double publicCost;

	public PublicMedicine(String name, Integer stock, Integer lowest, Double surgeryCost, Double publicCost) {
		super(name, stock, lowest, surgeryCost);

		this.publicCost = publicCost;
	}

	
	public void setStock(Integer i) {
		this.stock = i;
	}
	

	
	public Double getPublicCost() {
		return this.publicCost;
	}

	// ToDo : get / set Methods ?
}

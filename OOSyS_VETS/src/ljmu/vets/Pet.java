package ljmu.vets;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Pet implements PetInterface, Serializable {
	//defining variables
	protected String name;
	protected LocalDate regDate;

	// creating an array to store bookings
	protected List<Booking> bookings = new ArrayList<Booking>();
	
	//getting the name and registration date
	public Pet(String name, LocalDate regDate) {
		this.name = name;
		this.regDate = regDate;
	}
	
	
	//method to make a new booking
	public void makeBooking(BookingInterface when) {
		this.bookings.add((Booking) when);
	}

	//methhod to get the next booking
	public BookingInterface getNextBooking() {
		return this.bookings.stream()
			.filter(o -> o.getWhen().isAfter(LocalDateTime.now()))
				.sorted(Comparator.comparing(o -> o.getWhen()))
					.findFirst().get();
	}

		//gets name
	public String getName() {
		return this.name;
	}

	//gets reg date
	public LocalDate getRegDate() {
		return this.regDate;
	}
	
	//converts name and reg date to string
	public String toString() {
        return this.getClass().getSimpleName() + " >> " +
                this.name + " " +
                this.regDate.format(DateTimeFormatter.ofPattern("dd MMM yy"));
    }

	// ToDo : get / set Methods ?
}


